import random
import time
from itertools import product

import numpy as np


def parser():
    num_items = int(input().split()[-1])
    capacity = int(input().split()[-1])
    _ = input()
    profits = []
    weights = []
    for i in range(num_items):
        _, profit, weight = map(int, input().split())
        profits.append(profit)
        weights.append(weight)

    return capacity, num_items, np.array(profits), np.array(weights)


def get_random_solution(c, n, p, w):
    s = np.random.choice([True, False], n)
    while type(evaluate(s, c, p, w)) is int:
        s[random.randint(0, n-1)] = False
    return s


def evaluate(solution, capacity, profits, weights):
    mask = solution
    sum_profit = profits[mask].sum()
    sum_weight = weights[mask].sum()
    if sum_weight > capacity:
        return -1

    return sum_profit, sum_weight


def evaluate_inc(cur_sol, cur_obj, op, capacity, profits, weights):
    if cur_sol[op] == True:
        return cur_obj[0] - profits[op], cur_obj[1] + weights[op]
    if cur_sol[op] == False:
        return cur_obj[0] + profits[op], cur_obj[1] - weights[op]


def constructive_heuristic(capacity, num_items, profits, weights):
    # gready algorithm
    desc_order = np.argsort(profits)[::-1]
    mask = np.full_like(profits, False, dtype=bool)

    current_weight = 0
    for i in desc_order:
        if current_weight + weights[i] <= capacity:
            mask[i] = True
            current_weight += weights[i]

    return mask


def get_neighborhoods(cur_sol, cur_obj, capacity, profits, weights):
    candidates = []
    for i in range(0, len(cur_sol)):
        c = cur_sol.copy()
        if c[i] == True:
            c[i] = False
        else:
            c[i] = True

        obj = evaluate(c, capacity, profits, weights)
        if type(obj) is int:
            continue
        candidates.append((c, obj))

    return candidates


def choice_candidates(cur_sol, cur_obj, sols, capacity, strategy='first'):
    if strategy == 'first':
        for sol in sols:
            if sol[1][1] <= capacity and sol[1][0] > cur_obj[0]:
                return sol
    elif strategy == 'best':
        best_sol = (cur_sol, cur_obj)
        for sol in sols:
            if sol[1][1] <= capacity and sol[1][0] > best_sol[1][0]:
                best_sol = sol
        if cur_obj != best_sol[1]:
            return best_sol
    else:
        raise NotImplementedError

    return -1


def local_search(init_sol, capacity, num_items, profits, weights, strategy='first'):
    cur_sol = init_sol
    cur_obj = evaluate(cur_sol, capacity, profits, weights)

    while True:
        sols = get_neighborhoods(cur_sol, cur_obj, capacity, profits, weights)
        x = choice_candidates(cur_sol, cur_obj, sols, capacity, strategy)
        if x == -1:
            return cur_obj, cur_obj
        cur_sol, cur_obj = x

def gen_init_sol(c, n, p, w, mode='random'):
    if mode == 'random':
        return get_random_solution(c, n, p, w)
    elif mode == 'gready':
        return constructive_heuristic(c, n, p, w)
    else:
        raise NotImplementedError


def main():
    capacity, num_items, profits, weights = parser()

    num_runs = 10
    strategies = ['first', 'best']
    modes = ['random', 'gready']
    for strategy, mode in product(strategies, modes):
        for i in range(num_runs):
            ts = time.time()
            init_sol = gen_init_sol(capacity, num_items, profits, weights, mode)
            sol, obj = local_search(init_sol, capacity, num_items, profits, weights, strategy)
            t = time.time() - ts
            print(f'{strategy} strategy, {mode} mode, random init {i}-th run: {obj}')
            print(f'Elapse time: {t}')

if __name__ == '__main__':
    main()
