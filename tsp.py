import time
from math import ceil
from itertools import product

import numpy as np


def dist(a, b):
    # distance by eculid
    return ceil(np.sqrt((a[0] - b[0])*(a[0] - b[0]) + (a[1] - b[1])*(a[1] - b[1])))


def parser():
    num_nodes = int(input().split()[-1])
    best_known = int(input().split()[-1])
    _ = input()
    xs = []
    ys = []
    for i in range(num_nodes):
        _, x, y = map(int, input().split())
        xs.append(x)
        ys.append(y)

    dists = np.zeros((num_nodes, num_nodes))
    for i in range(num_nodes):
        for j in range(num_nodes):
            dists[i][j] = dists[j][i] = dist((xs[i], ys[i]), (xs[j], ys[j]))

    return num_nodes, dists, best_known


def get_random_solution(n):
    return np.concatenate([np.array([0]), np.random.permutation(list(range(1, n)))])


def evaluate(solution, dists):
    return np.sum(dists[solution, np.concatenate([solution[1:], solution[0:1]])])


def evaluate_inc(cur_sol, cur_obj, op, dists):
    num_nodes = len(cur_sol)
    diff = 0
    inc = 0    
    for o, p in zip(op, op[::-1]):
        if o != 0:
            diff += dists[cur_sol[o]][cur_sol[o-1]]
        if p != 0:
            inc += dists[cur_sol[p]][cur_sol[p-1]]
        if o != num_nodes-1:
            diff += dists[cur_sol[o]][cur_sol[o+1]]
        if p != num_nodes-1:
            inc += dists[cur_sol[p]][cur_sol[p+1]]
    return cur_obj - diff + inc


def get_neighborhoods(cur_sol, cur_obj, dists):
    candidates = []  # (sol, operation, objective)
    for i in range(0, len(cur_sol)):
        for j in range(i+1, len(cur_sol)):
            c = cur_sol.copy()
            c[i], c[j] = c[j], c[i]
            objective = evaluate(c, dists)
            candidates.append((c, (i,j), objective))
    return candidates


def choice_candidates(cur_sol, cur_obj, sols, strategy='first'):
    if strategy == 'first':
        for sol in sols:
            if sol[2] < cur_obj:
                return sol
    elif strategy == 'best':
        best_sol = (cur_sol, None, cur_obj)
        for sol in sols:
            if sol[2] < best_sol[2]:
                best_sol = sol
        if cur_obj != best_sol[2]:
            return best_sol
    else:
        raise NotImplementedError

    return -1


def local_search(init_sol, dists, strategy='first'):
    cur_sol = init_sol
    cur_obj = evaluate(init_sol, dists)

    while True:
        sols = get_neighborhoods(cur_sol, cur_obj, dists)
        x = choice_candidates(cur_sol, cur_obj, sols, strategy=strategy)
        if x == -1:
            return cur_sol, cur_obj
        cur_sol, _, cur_obj = x

def constructive_heuristic(dists):
    # nearest neignbor
    d = dists.copy()
    num_nodes = dists.shape[0]
    res = [0]
    d[:, 0] = np.finfo(d[0, 0]).max
    for i in range(num_nodes-1):
        next_node = np.argmin(d[res[-1]])
        res.append(next_node)
        d[:, next_node] = np.finfo(d[0, next_node]).max

    return np.array(res)


def gen_init_sol(num_nodes, dists, mode='random'):
    if mode == 'random':
        return get_random_solution(num_nodes)
    elif mode == 'gready':
        return constructive_heuristic(dists)
    else:
        raise NotImplementedError


if __name__ == '__main__':
    num_nodes, dists, _ = parser()

    num_runs = 10
    strategies = ['first', 'best']
    modes = ['random', 'gready']
    for strategy, mode in product(strategies, modes):
        for i in range(num_runs):
            ts = time.time()
            init_sol = gen_init_sol(num_nodes, dists, mode)
            sol, obj = local_search(init_sol, dists, strategy)
            t = time.time() - ts
            print(f'{strategy} strategy, {mode} mode, random init {i}-th run: {obj}')
            print(f'Elapse time: {t}')
